package it.unica.pr2.progetto2015.g48982_48909_49365.semplice;

import java.lang.Math;  

public class Semplice implements it.unica.pr2.progetto2015.Interfacce.SheetFunction
{
	//Semplice(){}
	/*questo metodo prende in ingresso un double, che rappresenta i radianti e calcola i gradi*/
	public Object execute(Object... args)
        {
		//Double radianti = Double.valueOf((String)args [0]);
		
		Double radianti = (Double)args [0];  
		
		Double gradi = new Double ((180*radianti)/Math.PI);  //calcolo GRADI
		
		
		return gradi;
	}
	
	public String getCategory()  
        {
		return "Matematica";
	}

	public String getHelp()
        {
		return "Converte i radianti in gradi";
	}
	
	public String getName()
        {
		return "GRADI";
	}

}
