/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48982_48909_49365.custom;
import java.util.*;

/**
 *
 * @author stefano
 */
class Lingua 
{
    private HashMap<String, String> lingue;  //dichiaro una mappa che conterrà tutte le lingue
           
    /*costruttore che richama il metodo creaMappa*/
    Lingua()
    {
        creaMappa();
    }
    
    /*questo metodo popola la mappa in cui il nome della lingua è la chiave, e il valore è l'abbreviazione
     che serve per la funzione della libreria Translator.translate
    */
    private void creaMappa()
    {
        lingue = new HashMap<String, String>();
        lingue.put("AFRIKAANS", "af");
        lingue.put("ALBANIAN","sq");
        lingue.put("ARABIC","ar");
        lingue.put("ARMENIAN","hy");
        lingue.put("AZERBAIJANI","az");
        lingue.put("BASQUE","eu");
        lingue.put("BELARUSIAN","be");
        lingue.put("BENGALI","bn");
        lingue.put("BULGARIAN","bg");
        lingue.put("CATALAN","ca");
        lingue.put("CHINESE","zh-CN");
        lingue.put("CHINESE_SIMPLIFIED","zh-CN");
        lingue.put("CHINESE_TRADITIONAL","zh-TW");
        lingue.put("CROATIAN","hr");
        lingue.put("CZECH","cs");
        lingue.put("DANISH","da");
        lingue.put("DUTCH","nl");
        lingue.put("ESTONIAN","et");
        lingue.put("FILIPINO","tl");
        lingue.put("FINNISH","fi");
        lingue.put("FRENCH","fr");
        lingue.put("GALICIAN","gl");
        lingue.put("GEORGIAN","ka");
        lingue.put("GERMAN","de");
        lingue.put("GREEK","el");
        lingue.put("GUJARATI","gu");
        lingue.put("HAITIAN_CREOLE","ht");
        lingue.put("HEBREW","iw");
        lingue.put("HINDI","hi");
        lingue.put("HUNGARIAN","hu");
        lingue.put("ICELANDIC","is");
        lingue.put("INDONESIAN","id");
        lingue.put("IRISH","ga");
        lingue.put("ITALIAN","it");
        lingue.put("JAPANESE","ja");
        lingue.put("KANNADA","kn");
        lingue.put("KOREAN","ko");
        lingue.put("LATIN","la");
        lingue.put("LATVIAN","lv");
        lingue.put("LITHUANIAN", "lt");
        lingue.put("MACEDONIAN", "mk");
        lingue.put("MALAY", "ms");
        lingue.put("MALTESE", "mt");
        lingue.put("NORWEGIAN", "no");
        lingue.put("PERSIAN", "fa");
        lingue.put("POLISH", "pl");
        lingue.put("PORTUGUESE", "pt");
        lingue.put("ROMANIAN", "ro");
        lingue.put("RUSSIAN", "ru");
        lingue.put("SERBIAN", "sr");
        lingue.put("SLOVAK", "sk");
        lingue.put("SLOVENIAN", "sl");
        lingue.put("SPANISH", "es");
        lingue.put("SWAHILI", "sw");
        lingue.put("SWEDISH", "sv");
        lingue.put("TAMIL", "ta");
        lingue.put("TELUGU", "te");
        lingue.put("THAI", "th");
        lingue.put("TURKISH", "tr");
        lingue.put("UKRAINIAN", "uk");
        lingue.put("URDU", "ur");
        lingue.put("VIETNAMESE", "vi");
        lingue.put("WELSH", "cy");
        lingue.put("YIDDISH", "yi");
        lingue.put("ENGLISH", "en");
    }
    
    /*questo metodo controlla che la lingua sia presente nella mappa. Verifica che sia stata scritta per intero
    o per abbreviazione
    */
    String trovaLingua(String cercata)
    { 
        if(lingue.containsValue(cercata))  //se abbreviazione 
        {
            return cercata;
        }
        else if(lingue.containsKey(cercata))  //se nome completo
        {
            return lingue.get(cercata);
        }
        else  //altrimenti messaggio di errore
        {
            throw new RuntimeException("lingua non trovata. Prova a scrivere la lingua in inglese e tutto in maiuscolo, es. ITALIAN");
        }
    }
}
