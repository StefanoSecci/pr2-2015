package it.unica.pr2.progetto2015.g48982_48909_49365.Complessa;
import java.util.*;
public class Complessa
{
	/*prende in ingresso una matrice di stringhe per i database, un intero o una stringa per il campo del database
          e una matrice di stringhe per i criteri di ricerca 
         */
	public Object execute(Object... args)
        {
            String[][] matrDatabase;
            String[][] matrCriteri;
            String campo;
            int colonnaCampo;
            int i = 0;
            Integer count = new Integer(0);  //contatore per il contaValori
            Database database; 
            CriterioList lista; 
            
            try
            {
                /*verifico che il primo argomento sia matrice di stringhe*/
                if(args[0] instanceof String[][])
                {
                    matrDatabase = (String[][])args[0];
                }else{
                    throw new RuntimeException("il database deve essere una matrice di stringhe");
                }

                /*altre verifiche sugli input*/
                if(args[1] instanceof String && args[2] instanceof String[][])  
                {
                    /*caso in cui il campo indicato come stringa*/
                    campo = (String)args[1];
                    matrCriteri = (String[][])args[2];
                    database = new Database(matrDatabase, campo);
                }else if(args[1] instanceof Integer && args[2]instanceof String[][])  
                {
                    /*caso in cui viene passata la colonna del campo*/
                        colonnaCampo = ((Integer)args[1])-1;
                        matrCriteri = (String[][])args[2];
                        database = new Database(matrDatabase, colonnaCampo);
                }else if(args[1] instanceof String[][])
                {
                    /*caso in cui il campo non è indicato*/
                    matrCriteri = (String[][])args[1];
                    database = new Database(matrDatabase);

                }else{
                    throw new RuntimeException("sono richieste, oltre al database, una stringa o un intero che rappresenti il campo e una matrice di stringhe che rappresenti i criteri di ricerca");
                }
                /*la classe CriterioList comprime la matrice dei criteri in una lista per facilitarne la lettura*/ 
                lista = new CriterioList(database.getIntestazione(), matrCriteri); 
                count = contaValori(database,lista);  //verifica dei criteri e conteggio risultato
            }
            catch(RuntimeException e)
            {
                return "ERRORE: "+e.getMessage();
            }
            return count;
	}
        
        /*
        prende in ingresso un vettore di stringhe che rappresenta un record del database e
        la lista dei criteri e restituisce true se il record soddisfi i criteri.
        
        */
        private boolean verificaValori(String[] dato, CriterioList criteri)
        {
            boolean test = false;
            int i = 0;
            int index = 0;  //puntatore al campo del record
            String parametro;  //parametro di ricerca 
            String segno;  //si riferisce ad un argomento della classe Criterio e prende valori AND, OR, END
            Double paramDouble;  
            Double datoDouble;
            
            /*se la lista è vuota tutti i record non sono verificati*/
            if(criteri.size()<=0)
            {
                return false;
            }
            
           
            /*cicla finchè non trova l'argomento segno uguale a END che indica la fine della lista */
            do //!(criteri.get(i).prossimo).equals("END")
            {
                /* 
                cicla finchè l'operatore logico tra due parametri di ricerca è uguale a AND, 
                qualora il confronto dia esito negativo interrompe il ciclo e verifica la riga successiva della 
                tabella dei criteri indicata con segno OR
                */
                do//(!(criteri.get(i-1).prossimo).equals("OR") && !(criteri.get(i-1).prossimo).equals("END"))
                {
                    index = criteri.get(i).colonna;  //individua il campo dei criteri da verificare 
                    parametro = criteri.get(i).dato;  //individua il parametro da confrontare
                    segno = criteri.get(i).segno;  //leggo l'operatore logico per il confronto successivo

                    
                    try //controllo che il parametro sia un double 
                    {
                        paramDouble = Double.valueOf(parametro);
                        try  //nel caso anche il dato del database sia un double procedo con il confronto
                        {
                            datoDouble = Double.valueOf(dato[index]);
                            test = verifica(segno, datoDouble, paramDouble);
                        }catch (RuntimeException e)  //se sono di tipi diversi il controllo fallisce  
                        {
                            test = false;
                        }
                    }catch (RuntimeException f) //nel caso il parametro non sia un double...
                    {

                        try //... ma il dato lo sia il test fallisce 
                        {
                            datoDouble = Double.valueOf(dato[index]);
                            test = false;
                        }catch (RuntimeException g)  //... e anche il dato non sia un double procedo con la verifica 
                        {
                            test = verifica(segno, dato[index], parametro); 
                        }
                    }

                    i++; //incremento il puntatore
                    
                    if(test == false) //se un test fallisce salto tutti gli AND e mi porto alla riga successiva, interrompendo il ciclo
                    {
                        while(criteri.get(i-1).prossimo.equals("AND"))
                        {
                            i++;
                        }
                        break;
                    }
                      
                }while(!(criteri.get(i-1).prossimo).equals("OR") && !(criteri.get(i-1).prossimo).equals("END"));//secondo while
              
                /*se una succesione di AND è verificata allora il record verifica le condizioni di ricerca*/
                if(test == true && !(criteri.get(i-1).prossimo).equals("END"))
                {
                    return true;
                }
                
            }while(!(criteri.get(i-1).prossimo).equals("END"));//primo while
            
            
           
            return test;
        }//fine metodo
        
        /*prende il segno del confronto (<,>,=,...) e due Comparable e li confronta*/
        private boolean verifica (String segno, Comparable datoDB, Comparable param)
        {
            int confronto = datoDB.compareTo(param);
            
            /*effettua il confronto specificato dal segno*/
            if(segno.equals(">") && confronto>0 || segno.equals(">=") && confronto >= 0
                    || segno.equals("=") && confronto == 0 || segno.equals("<") && confronto<0
                    || segno.equals("<=") && confronto <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        /*prende il database e la lista dei criteri e si occupa di contare 
        le volte in cui i vari record verifichino i criteri*/
        private Integer contaValori (Database database, CriterioList lista)
        {
            int i=0, cont=0;
            
            for(i = 0; i<database.getSize(); i++)
            {
                /*richiamo il metodo verificaValori per ogni record del database*/
                if(verificaValori(database.getRecord(i), lista))
                {
                    cont++;
                }
            }
            
            return cont;
        }
        
	public String getCategory()
        {
		return "Database";
	}

	public String getHelp()
        {
		return "Restituisce il numero delle righe che soddisfano i criteri di ricerca e che contengono valori numeri o alfanumerici";
	}
	
	public String getNome()
        {
		return "DB.CONTA.VALORI";
	}

}
