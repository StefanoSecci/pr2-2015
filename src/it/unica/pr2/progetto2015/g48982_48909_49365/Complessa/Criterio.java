/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48982_48909_49365.Complessa;

/**
 *
 * @author ercules
 */
class Criterio {
    public int colonna;  //indica la colonna del database in cui verificare il criterio
    public String segno;  //indica l'operatore logico per il confronto
    public String dato;  //indica il valore su cui effettuare il confronto
    public String prossimo;  //indica l'operatore logico (che può essere AND, OR, END) che lega due criteri di ricerca
    
    Criterio(int colonna, String segno, String dato)
    {
        this.colonna = colonna;
        this.segno = segno;
        this.dato = dato;
        this.prossimo = "AND";
    }
    
    void setOR()
    {
        this.prossimo = "OR";
    }
    
    void setEND()
    {
        this.prossimo = "END";
    }
}
