package it.unica.pr2.progetto2015.g48982_48909_49365.Complessa;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author ercules
 */
/*
classe che si occupa di semparare l'intestazione della tabella database
e mette tutti i record in una lista. ha tre costruttori 
*/
public class Database {
    private String[] intestazione;
    private ArrayList<String[]> lista = new ArrayList(); // lista dei record
    
    /* costruisce il database scartando i record che non hanno valori 
    nel campo passato come parametro */
    public Database(String[][] database, String colonna)
    {
        this.intestazione = database[0];
        int i=0;
        
        this.compilaLista(database); // insericso tutti i record nella lista
        
        // il campo nel database 
        for(i = 0; i<intestazione.length; i++)
        {
            
            if(colonna.equals(intestazione[i]))
            {
                /* se trovo il campo passo l'indice alla funzione che si 
                occupa di scartare i record non validi
                */
                this.adattaLista(i);
                break;
            }
        }
        
        // se non trovo il campo cercato lancio un'eccezione
        if(i>=intestazione.length)
        {
            throw new RuntimeException("colonna inesistente");
        }
    }
    
    /* costruisce il database scartando i record che non hanno valori 
    nel campo passato come parametro */
    public Database(String[][] database, int indiceCol)
    {
        this.intestazione = database[0];
        
        // verifico che l'indice sia valido
        if(indiceCol>=intestazione.length || indiceCol<0)
        {
            throw new RuntimeException("colonna inesistente");
        }
        
        this.compilaLista(database); //costruisco la lista dei record ...
        this.adattaLista(indiceCol); // ... e scarto quelli che non interessano
    }
    
    /* costruisce il database inserendo tutti i record nella lista */
    public Database(String[][] database)
    {
        this.intestazione = database[0];
        
        this.compilaLista(database);
    }
    
    /*
    funzione che si occupa di inserire le righe della matrice database
    in una lista
    */
    private void compilaLista(String[][] database)
    {
        for(int i = 1; i<database.length; i++)
        {
            lista.add(database[i]);
        }
    }
    
    /*
    funzione che cancella i record che hanno valori nulli nel campo indicato
    */
    private void adattaLista(int indiceCol)
    {
        int i=0;
        
        // cicla per ogni elemento della lista partendo dall'ultimo
        for(i = lista.size()-1; i>=0; i--)
        {
            // trova le celle nulle per il campo indicato e lo rimuove dalla lista
            if(lista.get(i)[indiceCol].isEmpty())
            {
                lista.remove(i);
            }
        }
        
    }
    
    public String[] getRecord (int i)
    {
        return lista.get(i);
    }
    
    public int getSize()
    {
        return lista.size();
    }
    
    public String[] getIntestazione()
    {
        return this.intestazione;
    }
}
