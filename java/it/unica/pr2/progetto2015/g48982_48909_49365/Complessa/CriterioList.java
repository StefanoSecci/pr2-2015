/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48982_48909_49365.Complessa;
import java.util.*;

/**
 *
 * @author stefano
 */
class CriterioList {
    private ArrayList<Criterio> criteri = new ArrayList();//lista che conterrà i criteri di ricerca
    private String[] intestazioneCriterio; // intestazione della tabella dei criteri
    
    // costruttore che si occupa di mettere i criteri nella lista
    CriterioList(String[] intestazioneDB, String[][] criteri)
    {
        ArrayList<Criterio> listaCriteri = new ArrayList();
        this.intestazioneCriterio = criteri[0];
        Criterio ultimo; // ultimo inserito nella lista
        String segno; // operatore logico per il confronto
        String dato; // valore per il confronto
        int dimUltimo; //posizione dell'ultimo elemento della list
        
        /*
         se nella matrice dei criteri ci sono dei campi non presenti
        nel database, lancia un'eccezione
        */
        confrontaIntestazioni(intestazioneDB);

        for(int i=1; i<criteri.length; i++) // righe
        {
            for(int k=0; k<criteri[i].length; k++) // colonne
            {
                // le celle vuote sono ignorate
                if(!criteri[i][k].isEmpty())
                {
                    // costruisco un oggetto di tipo Criterio e lo agiungo alla lista
                    ultimo=costruisciCriterio(criteri[i][k], intestazioneDB, intestazioneCriterio[k]);
                    listaCriteri.add(ultimo); 
                }//if
            }//for colonne
            
            //ad ogni riga corrisponde un OR
            if(listaCriteri.size()>0)
            {
                // imposto l'attributo prossimo a OR dell'ultimo elemento, se presente
                dimUltimo = listaCriteri.size()-1;
                ultimo = listaCriteri.get(dimUltimo);
                ultimo.setOR();
                listaCriteri.set(dimUltimo, ultimo); 
            }
        }//for righe
        
        /*
        cambio l'attributo prossimo dell'ultimo elemento (se presente)
        e lo imposto a END
        */
        if(listaCriteri.size()>0)
        {
            dimUltimo = listaCriteri.size()-1; 
            ultimo = listaCriteri.get(dimUltimo);
            ultimo.setEND();
            listaCriteri.set(dimUltimo, ultimo);
        }

        this.criteri=listaCriteri;

    }//costruttore

    // dimensione lista
    int size()
    {
        return this.criteri.size();
    }
    
    // lettura elementi
    Criterio get(int i)
    {
        return this.criteri.get(i);
    }
    
    /*
    funzione che si occupa di passare i giusti parametri al
    costruttore della classe Criterio
    */
    private Criterio costruisciCriterio(String s, String[] intestazioneDB, String colonnaCriterio)
    {
        String segno;
        String dato;
        
        /* divido il contenuto delle celle della matrice dei criteri
        in segno per il confronto e dato per il confronto*/
        if(s.startsWith(">=") || s.startsWith("<="))
        {
            segno = s.substring(0,2);
            dato = s.substring(2);
             
        }else if(s.startsWith(">") || s.startsWith("<") || s.startsWith("="))
        {
            segno = s.substring(0,1);
            dato = s.substring(1);
            
        }else
        {
            segno = "=";
            dato = s;
            
        }

        // cerco la colonna del campo del database su cui fare il confronto e la salvo
        for(int j=0; j<intestazioneDB.length; j++)
        {
            
            if(colonnaCriterio.equals(intestazioneDB[j]))
            {
                return new Criterio(j, segno, dato);
            }//if
        }//for instestazioneDB
        
        // se il ciclo viene completato significa che non è stato trovato il campo
        throw new RuntimeException("colonna non presente nel database");
    }//metodo
    
    /* verifica che i campi cercati nei criteri siano presenti anche nel database*/
    private void confrontaIntestazioni(String[] intestazioneDB)
    {
        boolean test=true;
        
        for(String crit: intestazioneCriterio)
        {
            test=false;
            for(String data: intestazioneDB)
            {
                if(crit.equals(data))
                {
                    test=true;
                    break;
                }
            }
            if(test==false)
            {
                throw new RuntimeException("il campo \""+crit+"\" non è presente nel database");
            }
        }
        
    }
    
}//class
