package it.unica.pr2.progetto2015.g48982_48909_49365.custom;
import com.gtranslate.*;  //importa le classi della libreria 'custom-translate-api.jar'

public class Custom implements it.unica.pr2.progetto2015.Interfacce.SheetFunction
{
	/*la funzione prende in ingresso tre stringhe che 
        rappresentano il testo da tradurre e due lingue
        */
	public Object execute(Object... args)
        {
            Translator traduttore = Translator.getInstance();  //dichiaro un traduttore
            Lingua l = new Lingua();  //istanza un oggetto di tipo Lingua che uso per verificare la correttezza degli input
            String testo;  //testo da tradurre
            String prefissoLingua;
            String traduzione;
            String linguaTesto;
            String linguaTraduzione;
            
            /*controllo che i tipi in ingresso siano correti e li inserisco nelle rispettive variabili*/
            if(args[0] instanceof String && args[1] instanceof String && args[2] instanceof String)
            {
                testo = (String)args[0];
                linguaTesto = (String)args[1];
                linguaTraduzione = (String)args[2];
            }
            else  //altrimenti stampa un messaggio di errore
            {
                return "i tipi dei parametri passati non sono corretti";
            }
            
            /*verifico l'eccezione*/
            try
            {
                /*richiamo il metodo translate del traduttore e gli passo il testo da tradurre e le due lingue*/
                traduzione = traduttore.translate(testo, l.trovaLingua(linguaTesto), l.trovaLingua(linguaTraduzione));
            }
            catch(RuntimeException e)  //in caso le stringhe che rappresentano le lingue siano sbagliate lancio un'eccezione
            {
                return e.getMessage();  //chiudo l'applicazione e stampo il messaggi di errore
            }
            return traduzione;
	}// execute
	
	public String getCategory()
        {
		return "Testo";
	}

	public String getHelp()
        {
		return "Traduce una stringa da una lingua ad un'altra";
	}
	
	public String getName()
        {
		return "GOOGLE.TRANSLATE";
	}

}// CUSTOM  
