Progetto2015

Gruppo 18:
60/61/48909 Stefano Secci
60/61/48982 Alex Tevere
60/61/49365 Andrea Pisaturo

Semplice:
funzione GRADI:  richiede l'inserimento di un double.
Descrizione: trasforma radianti in gradi



Complessa:
funzione DB.CONTA.VALORI:  richiede l'inserimento di una matrice di stringhe che è rappresentata in database, una stringa o un intero che rappresenta il campo database (facoltativo), e infine un'altra matrice di stringhe che rappresenta i criteri di ricerca.
Descrizione: restituisce il numero delle righe del database che soddisfano i criteri specificati.



Custom: 
funzione google.translate:  richiede l'inserimento di una stringa che rappresenta il testo da tradurre, un'altra stringa che rapprensenta la lingua del testo iniziale, e una terza stringa che rappresenta la lingua in cui si vuole tradurre.
Descrizione: restituisce la traduzione dalla lingua di partenza alla lingua di destinazione.
N.B: la libreria tronca tutte le frasi al primo segno di punteggiatura che trova. Le lingue vanno inserire in inglese e tutto maiuscolo, ad esempio: (ENGLISH), 
sarà fornito un elenco con tutti gli input per le lingue supportate (lingue.txt). 
Abbiamo verificato che non vengono stamapti i caratteri di alcune lingue.